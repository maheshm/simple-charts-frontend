"use client";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import React from "react";
import { Tabs, Tab } from "@mui/material";
import { usePathname } from "next/navigation";
import { useEffect } from "react";
import { useRouter } from "next/navigation";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";

//fetch saved charts of user and show them in a grid

const SavedChartsPage: React.FC = () => {
  const [activeTab, setActiveTab] = React.useState(0);
  const router = useRouter();
  const pathname = usePathname();
  const [savedCharts, setSavedCharts] = React.useState<any[]>([]);

  const handleTabChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setActiveTab(newValue);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const deleteChart = async (id: string) => {
    try {
      const response = await fetch(
        "https://simplecharts.maheshlabs.com/api/charts/delete-chart?id=" + id,
        {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        },
      );

      if (response.ok) {
        fetchData();
      }
    } catch (error) {
      console.error("Error deleting chart:", error);
    }
  };

  const fetchData = async () => {
    try {
      const response = await fetch(
        "https://simplecharts.maheshlabs.com/api/charts?email=" +
          localStorage.getItem("email"),
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        },
      );

      const data = await response.json();
      setSavedCharts(data);
      console.log(data);
    } catch (error) {
      console.error("Error fetching saved charts:", error);
    }
  };
  return (
    <DefaultLayout>
      <div>
        <div className="max-w-270 text-left">
          <Breadcrumb pageName="Choose Data" />
        </div>
        <p>Here are the charts you've saved. Click on a chart to view it.</p>
        <br />
      </div>

      {!localStorage.getItem("email") && !localStorage.getItem("token") && (
        <div>
          <h2>
            <strong>Please login to view saved charts</strong>
          </h2>
        </div>
      )}
      <div className="grid grid-cols-1 justify-center  gap-4 md:grid-cols-2 md:gap-6 xl:grid-cols-4 2xl:gap-7.5">
        {savedCharts.map((chart) => (
          <div
            key={chart.id}
            className="rounded-lg bg-white p-4 shadow-lg dark:border-form-strokedark dark:bg-form-input dark:text-white"
          >
            {/* Image of the chart from /images/cards/{chart.type}.png and and blur to it*/}
            <img
              src={`/images/cards/${chart.type}.png`}
              className="h-32 w-full rounded-lg object-cover"
              alt={chart.name}
            />
            <h3 className="text-lg font-semibold">{chart.title}</h3>
            <p className="text-gray-500 text-sm">{chart.type}</p>
            <p className="text-gray-500 text-sm">
              Dataset: {chart.dataset.name}
            </p>

            <p className="text-gray-500 text-sm">
              Last Modified: {new Date(chart.lastModifiedOn).toDateString()}
            </p>
            <div className="mt-4 flex justify-between">
              <button
                className="rounded-lg bg-primary px-4 py-2 text-white"
                onClick={() =>
                  router.push(
                    `/chart/chart-workspace/${chart.type}?id=${chart.dataset.id}&chartTitle=${chart.title}&selectedXAxis=${chart.xaxisParameter}&selectedYAxis=${chart.yaxisParameter}&xAxisLabel=${chart.xaxisTitle}&yAxisLabel=${chart.yaxisTitle}`,
                  )
                }
              >
                View
              </button>
              <button
                className="rounded-lg bg-danger px-4 py-2 text-white"
                onClick={() => deleteChart(chart.id)}
              >
                Delete
              </button>
            </div>
          </div>
        ))}
      </div>
    </DefaultLayout>
  );
};

export default SavedChartsPage;
