"use client";
import DefaultLayout from '@/components/Layouts/DefaultLayout';
import { useState, useEffect } from 'react';
import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const BarChartComponent: React.FC = () => {
  const [csvData, setCsvData] = useState<string>('');
  const [parameters, setParameters] = useState<string[]>([]);
  const [selectedXAxis, setSelectedXAxis] = useState<string>('');
  const [selectedYAxis, setSelectedYAxis] = useState<string>('');

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('https://simplecharts.maheshlabs.com/api/datasets/download?id=' + new URLSearchParams(window.location.search).get('id'));
      const data = await response.text();
      setCsvData(data);
      const parsedData = parseCsvData(data);
      const parameters = Object.keys(parsedData[0]);
      console.log('parameters:', parameters)
      console.log('parsedData:', Object.values(parsedData[1]))
      setParameters(parameters);
      setSelectedXAxis(parameters[0]); // Set the default selected X axis parameter
      setSelectedYAxis(parameters[1]); // Set the default selected Y axis parameter
    } catch (error) {
      console.error('Error fetching CSV data:', error);
    }
  };

  const parseCsvData = (csvData: string): any[] => {
    const [header, ...rows] = csvData.split('\n');
    const parameters = header.split(',');
  
    return rows.map((row) => {
      const columns = row.split(',');
      const rowData: any = {};
  
      parameters.forEach((parameter, index) => {
        if(columns[index] !== undefined){
          rowData[parameter] = columns[index].trim();
        }
      });
  
      return rowData;
    });
  };

  const handleChartGeneration = () => {
    if (!csvData || !selectedXAxis || !selectedYAxis || !parameters.includes(selectedXAxis) || !parameters.includes(selectedYAxis)) {
      return [];
    }
  
    const data = parseCsvData(csvData);
  
    // Group data by x-axis value and sum the corresponding y-axis values
    const groupedData = data.reduce((accumulator: any, currentValue: any) => {
        const xValue = "Article" + Math.floor(Math.random() * 10) + 1;
      //const xValue = currentValue[selectedXAxis];
      const yValue = parseFloat(currentValue[selectedYAxis]);
  
      if (!accumulator[xValue]) {
        accumulator[xValue] = 0;
      }
      accumulator[xValue] += yValue;
  
      return accumulator;
    }, {});
  
    // Convert grouped data back to array format
    const filteredData = Object.keys(groupedData).map((xValue) => ({
      x: xValue,
      y: groupedData[xValue],
    }));
  
    return filteredData;
  };

  return (
    <DefaultLayout>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <h2>Select Parameters</h2>
          <select value={selectedXAxis} onChange={(e) => setSelectedXAxis(e.target.value)}>
            {parameters.map((param) => (
              <option key={param} value={param}>
                {param}
              </option>
            ))}
          </select>
          <select value={selectedYAxis} onChange={(e) => setSelectedYAxis(e.target.value)}>
            {parameters.map((param) => (
              <option key={param} value={param}>
                {param}
              </option>
            ))}
          </select>
        </div>
        <div style={{ flex: 2, width: '400px', height: '400px' }}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart data={handleChartGeneration()}>
              <XAxis dataKey="x" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="y" fill={`#${Math.floor(Math.random() * 16777215).toString(16)}`} />
            </BarChart>
          </ResponsiveContainer>
        </div>
      </div>
    </DefaultLayout>
  );
};

export default BarChartComponent;
