"use client";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import { Button } from "@mui/material";
import React, { useState, useEffect, useRef } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
  CartesianGrid,
  Label,
} from "recharts";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";

const LineChartComponent: React.FC = () => {
  const [csvData, setCsvData] = useState<string>("");
  const [parameters, setParameters] = useState<string[]>([]);
  const [selectedXAxis, setSelectedXAxis] = useState<string>("");
  const [selectedYAxis, setSelectedYAxis] = useState<string>("");

  const [chartTitle, setChartTitle] = useState("");
  const [xAxisLabel, setXAxisLabel] = useState("");
  const [yAxisLabel, setYAxisLabel] = useState("");

  const chartRef = useRef<any>(null);

  useEffect(() => {
    fetchData();
    const url = new URL(window.location.href);
    const chartTitle = url.searchParams.get("chartTitle");
    const selectedXAxis = url.searchParams.get("selectedXAxis");
    const selectedYAxis = url.searchParams.get("selectedYAxis");
    const xAxisLabel = url.searchParams.get("xAxisLabel");
    const yAxisLabel = url.searchParams.get("yAxisLabel");

    if (xAxisLabel) {
      setXAxisLabel(xAxisLabel);
    }
    if (yAxisLabel) {
      setYAxisLabel(yAxisLabel);
    }

    if (chartTitle) {
      setChartTitle(chartTitle);
    }
    if (selectedXAxis) {
      setSelectedXAxis(selectedXAxis);
    }
    if (selectedYAxis) {
      setSelectedYAxis(selectedYAxis);
    }
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch(
        "https://simplecharts.maheshlabs.com/api/datasets/download?id=" +
          new URLSearchParams(window.location.search).get("id"),
      );
      const data = await response.text();
      setCsvData(data);
      const parsedData = parseCsvData(data);
      const parameters = Object.keys(parsedData[0]);
      console.log("parameters:", parameters);
      console.log("parsedData:", Object.values(parsedData[1]));
      setParameters(parameters);
      setSelectedXAxis(parameters[0]); // Set the default selected X axis parameter
      setSelectedYAxis(parameters[1]); // Set the default selected Y axis parameter
    } catch (error) {
      console.error("Error fetching CSV data:", error);
    }
  };

  const parseCsvData = (csvData: string): any[] => {
    const [header, ...rows] = csvData.split("\n");
    const parameters = header.split(",");

    return rows.map((row) => {
      const columns = row.split(",");
      const rowData: any = {};

      parameters.forEach((parameter, index) => {
        if (columns[index] !== undefined) {
          rowData[parameter] = columns[index].trim();
        }
      });

      return rowData;
    });
  };

  const handleChartGeneration = () => {
    if (
      !csvData ||
      !selectedXAxis ||
      !selectedYAxis ||
      !parameters.includes(selectedXAxis) ||
      !parameters.includes(selectedYAxis)
    ) {
      return [];
    }

    const data = parseCsvData(csvData);

    // Group data by x-axis value and sum the corresponding y-axis values
    const groupedData = data.reduce((accumulator: any, currentValue: any) => {
      // const xValue = "Article" + Math.floor(Math.random() * 10) + 1;
      const xValue = currentValue[selectedXAxis];
      const yValue = parseFloat(currentValue[selectedYAxis]);

      if (!accumulator[xValue]) {
        accumulator[xValue] = 0;
      }
      accumulator[xValue] += yValue;

      return accumulator;
    }, {});

    // Convert grouped data back to array format
    const filteredData = Object.keys(groupedData).map((xValue) => ({
      x: xValue,
      y: groupedData[xValue],
    }));

    return filteredData;
  };
  const downloadChart = () => {
    const svgString = new XMLSerializer().serializeToString(
      chartRef.current.container.querySelector("svg") as Node,
    );

    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");

    if (ctx) {
      const img = new Image();
      img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        // add backbround color
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0);

        const dataUrl = canvas.toDataURL("image/png");
        const link = document.createElement("a");
        link.href = dataUrl;
        //if chart title is null then set default name
        link.download = (chartTitle ? chartTitle : "chart") + ".png";
        link.click();
      };
      img.src =
        "data:image/svg+xml;base64," +
        btoa(unescape(encodeURIComponent(svgString)));
    } else {
      console.error("Unable to get 2D rendering context from canvas.");
    }
  };

  const saveChart = async () => {
    if (localStorage.getItem("email") === null) {
      alert("Please login to save the chart");
      return;
    }
    try {
      const response = await fetch(
        "https://simplecharts.maheshlabs.com/api/charts/save",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Bearer: localStorage.getItem("token") || "",
          },
          body: JSON.stringify({
            title: chartTitle,
            graphConfiguration: "string",
            type: "line-chart",
            datasetId: Number(
              new URLSearchParams(window.location.search).get("id"),
            ),
            userEmail: localStorage.getItem("email"),
            xaxisTitle: xAxisLabel,
            yaxisTitle: yAxisLabel,
            xaxisParameter: selectedXAxis,
            yaxisParameter: selectedYAxis,
          }),
        },
      );
      const data = await response.json();
      console.log(data);
      if (response.ok) {
        alert("Chart saved successfully");
      }
    } catch (error) {
      console.error("Error saving chart:", error);
    }
  };

  return (
    <DefaultLayout>
      <div className="max-w-270 text-left">
        <Breadcrumb pageName="Line Chart" />
      </div>
      <div style={{ display: "flex" }}>
        <div style={{ flex: 1 }}>
          <div style={{ flex: 1, marginRight: "20px" }}>
            <div style={{ marginBottom: "20px" }}>
              <h1 style={{ marginBottom: "10px" }}>Customize &amp; Interact</h1>
              <p>
                Click &amp; drag on the chart to pan. Scroll to zoom. Hover over
                chart elements to see their respective values. Hover over
                datasets in the legend to bring them into focus, or click them
                to hide/show them.
              </p>
            </div>
            <div style={{ marginBottom: "10px" }}>
              <h3 style={{ marginBottom: "5px" }}>Chart Title</h3>
              <div style={{ display: "flex", alignItems: "center" }}>
                <input
                  type="text"
                  placeholder="chart title"
                  style={{ flex: 1, marginRight: "5px" }}
                  value={chartTitle}
                  onChange={(e) => setChartTitle(e.target.value)}
                />
              </div>
            </div>
            <div style={{ marginBottom: "10px" }}>
              <h3 style={{ marginBottom: "5px" }}>X-Axis Label</h3>
              <div style={{ display: "flex", alignItems: "center" }}>
                <input
                  type="text"
                  placeholder="x-axis label"
                  style={{ flex: 1, marginRight: "5px" }}
                  value={xAxisLabel}
                  onChange={(e) => setXAxisLabel(e.target.value)}
                />
                {/* <span style={{ cursor: 'pointer' }}>?</span> */}
                <select
                  value={selectedXAxis}
                  onChange={(e) => setSelectedXAxis(e.target.value)}
                >
                  {parameters.map((param) => (
                    <option key={param} value={param}>
                      {param}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div style={{ marginBottom: "10px" }}>
              <h3 style={{ marginBottom: "5px" }}>Y-Axis Label</h3>
              <div style={{ display: "flex", alignItems: "center" }}>
                <input
                  type="text"
                  placeholder="y-axis label"
                  style={{ flex: 1, marginRight: "5px" }}
                  value={yAxisLabel}
                  onChange={(e) => setYAxisLabel(e.target.value)}
                />
                <select
                  value={selectedYAxis}
                  onChange={(e) => setSelectedYAxis(e.target.value)}
                >
                  {parameters.map((param) => (
                    <option key={param} value={param}>
                      {param}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div style={{ marginBottom: "10px" }}>
              <h3 style={{ marginBottom: "5px" }}>Color Style</h3>
              <div style={{ display: "flex", alignItems: "center" }}>
                <select style={{ flex: 1, marginRight: "5px" }}>
                  <option>Pick a style</option>
                </select>
              </div>
            </div>
            <div style={{ marginBottom: "10px" }}>
              <h3 style={{ marginBottom: "5px" }}>Hide Legend</h3>
              <input type="checkbox" style={{ marginRight: "5px" }} />
            </div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: "20px",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                style={{ marginRight: "10px" }}
                onClick={downloadChart}
              >
                Download Chart
              </Button>
              <Button variant="contained" color="primary" onClick={saveChart}>
                Save Chart
              </Button>
            </div>
          </div>
        </div>
        {/* Add chartTitle on top on chart */}

        <div
          id="chart-container"
          style={{ flex: 2, backgroundColor: "#f9f9f9", padding: "20px" }}
        >
          <ResponsiveContainer width={800} height={400}>
            <LineChart
              ref={chartRef}
              data={handleChartGeneration()}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <text x="50%" y={15} textAnchor="middle" fontSize="16">
                {chartTitle}
              </text>

              <CartesianGrid strokeDasharray="3 3" />
              <XAxis
                dataKey="x"
                label={{
                  value: xAxisLabel,
                  position: "insideBottom",
                  offset: -4,
                }}
              />
              <YAxis
                dataKey="y"
                label={{
                  value: yAxisLabel,
                  angle: -90,
                  position: "insideLeft",
                }}
              />
              <Tooltip />

              <Line
                type="monotone"
                dataKey="x"
                stroke="#8884d8"
                fill={`#${Math.floor(Math.random() * 16777215).toString(16)}`}
              />
              <Line type="monotone" dataKey="y" stroke="#82ca9d" />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
    </DefaultLayout>
  );
};

export default LineChartComponent;
