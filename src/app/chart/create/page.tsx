"use client";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import ChooseChartPage from "../../../components/choose-chart-sample/page";
import UploadFilePage from "../../../components/UploadFile/fileUpload";
import React from "react";
import { Tabs, Tab } from "@mui/material";
import { usePathname } from "next/navigation";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";

const CreateChartPage: React.FC = () => {
  const [activeTab, setActiveTab] = React.useState(0);
  const pathname = usePathname();

  const handleTabChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setActiveTab(newValue);
  };
  return (
    <DefaultLayout>
      <div>
        <div className="max-w-270 text-left">
          <Breadcrumb pageName="Choose Data" />
        </div>
        <p>
          Formatting matters. The quality of a visualization can only ever match
          the quality of its data. Click on a sample from our simple or complex
          datasets to see how SimpleChartsRI can help you visualize data, or
          create your own dataset.
        </p>
        <br />
        <Tabs
          value={activeTab}
          onChange={handleTabChange}
          aria-label="Choose Data tabs"
        >
          {/* Make below tabs bold */}

          <Tab label="Choose Sample" />
          <Tab label="Create Data" />
          <Tab label="Upload File" />
        </Tabs>

        <div role="tabpanel" hidden={activeTab !== 0}>
          {/* Content for Choose Sample tab */}
          <ChooseChartPage />
          {/* Add grid to display images */}
        </div>

        <div role="tabpanel" hidden={activeTab !== 1}>
          {/* Content for Create Data tab */}
          <h3>Create Data</h3>
          {/* Add form to create CSV data */}
        </div>

        <div role="tabpanel" hidden={activeTab !== 2}>
          {/* Content for Upload File tab */}
          <UploadFilePage />
          {/* Add file upload input */}
        </div>
      </div>
    </DefaultLayout>
  );
};

export default CreateChartPage;
