import ECommerce from "@/components/Dashboard/E-commerce";
import Analytics from "@/components/Dashboard/Analytics";
import DefaultLayout from "@/components/Layouts/DefaultLayout";


export default function Home() {
  return (
    <>
      <DefaultLayout>
        <Analytics />
      </DefaultLayout>
    </>
  );
}
