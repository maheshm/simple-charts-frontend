
"use client";
import { Metadata } from "next";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import LandingPage from "@/components/Dashboard/Landing-Page";
import { Card, CardContent, Grid, makeStyles, Typography } from "@mui/material";
import { Button } from "@mui/material";
import React, { use } from "react";



export default function Home() {
  return (
    <>
      <DefaultLayout>
        <LandingPage></LandingPage>
      </DefaultLayout>
    </>
  );
}
