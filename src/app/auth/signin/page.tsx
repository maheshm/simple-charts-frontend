import Link from "next/link";
import Image from "next/image";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import { Metadata } from "next";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import React from "react";
import LoginForm from "@/components/Auth/LoginForm";

export const metadata: Metadata = {
  title: "Next.js SignIn Page | TailAdmin - Next.js Dashboard Template",
  description: "This is Next.js Signin Page TailAdmin Dashboard Template",
};

const SignIn: React.FC = () => {
  return (
      // move the block in the middle of the page
      // to the center of the page
      // by adding the classes flex, items-center, justify-center


     <div className="flex items-center justify-center h-screen">
    
      <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
        <div className="flex flex-wrap items-center">
          <div className="hidden w-full xl:block xl:w-1/2">
            <div className="px-26 py-17.5 text-center">    
              <h2 className="mb-9 text-2xl font-bold text-black dark:text-white sm:text-title-xl2">
                SimpleCharts
              </h2>
              <p className="2xl:px-20">
                SimpleCharts is a website that helps users create
                visualizations. It provides basic charting options as well as
                provides sample data and other material to learn from.
              </p>
            </div>
          </div>

          <div className="w-full border-stroke dark:border-strokedark xl:w-1/2 xl:border-l-2">
            <div className="w-full p-4 sm:p-12.5 xl:p-17.5">
              <h2 className="mb-9 text-2xl font-bold text-black dark:text-white sm:text-title-xl2">
                Sign In to SimpleCharts
              </h2>
              <LoginForm />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
