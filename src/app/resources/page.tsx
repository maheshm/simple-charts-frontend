"use client";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import React from "react";
import { Tabs, Tab } from "@mui/material";
import { usePathname } from "next/navigation";
import ChooseChartPage from "@/components/choose-chart-sample/page";
import ResourceTile from "@/components/Resources/resource-tile";

const Resources = () => {
  const [activeTab, setActiveTab] = React.useState(0);
  const [datasets, setDatasets] = React.useState<any[]>([]);
  const pathname = usePathname();

  React.useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    try {
      const response = await fetch(
        "https://simplecharts.maheshlabs.com/api/resource-category",
      );
      const data = await response.json();
      setDatasets(data);
    } catch (error) {
      console.error("Error fetching datasets:", error);
    }
  };

  const handleTabChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setActiveTab(newValue);
  };
  return (
    <DefaultLayout>
      <div className="max-w-270 text-left">
        <Breadcrumb pageName="Resources" />
      </div>

      <Tabs
        value={activeTab}
        onChange={handleTabChange}
        aria-label="Choose Data tabs"
      >
        {datasets.map((dataset, index) => (
          <Tab key={dataset.id} label={dataset.name} />
        ))}
      </Tabs>

      {datasets.map((dataset, index) => (
        <div
          key={dataset.id}
          style={{ display: activeTab === index ? "block" : "none" }}
        >
          <ResourceTile datasetId={dataset.id} />
        </div>
      ))}
    </DefaultLayout>
  );
};

export default Resources;
