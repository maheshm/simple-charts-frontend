import React, { useEffect, useState } from "react";
import { Card, CardContent, Typography } from "@mui/material";
import Link from "next/link";
import { usePathname } from "next/navigation";

interface ResourceTileProps {
  datasetId: string;
}

const ResourceTile: React.FC<ResourceTileProps> = ({ datasetId }) => {
  const [resources, setResources] = useState<any[]>([]);
  const pathname = usePathname();

  useEffect(() => {
    fetchData();
  }, [datasetId]);

  const fetchData = async () => {
    try {
      const response = await fetch(
        `https://simplecharts.maheshlabs.com/api/resources?resource-category-id=${datasetId}`
      );
      const data = await response.json();
      setResources(data);
    } catch (error) {
      console.error("Error fetching resources:", error);
    }
  };

  return (
    <div>
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {resources.map((resource) => (
          <Link href={`${resource.url}`} key={resource.id}>
            <Card
              key={resource.id}
              style={{
                margin: "10px",
                width: "300px",
                height: "200px", // Set a fixed height for all cards
                cursor: "pointer",
              }}
            >
              <CardContent
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                  textAlign: "center",
                  height: "100%", // Ensure CardContent fills the entire height
                }}
              >
                <Typography variant="h6" component="h2">
                  {resource.name}
                </Typography>
                <Typography color="textSecondary">
                  {resource.description}
                </Typography>
              </CardContent>
            </Card>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default ResourceTile;
