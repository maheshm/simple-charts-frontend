import React, { useEffect, useState } from 'react';
import { Card, CardContent, Typography, Grid, Paper } from '@mui/material';
import Link from 'next/link';
import { usePathname } from "next/navigation";
import SelectChart from './select-chart';

const ChooseChartPage: React.FC = () => {
  const [datasets, setDatasets] = useState<any[]>([]);
  const pathname = usePathname();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('https://simplecharts.maheshlabs.com/api/datasets');
      const data = await response.json();
      setDatasets(data);
    } catch (error) {
      console.error('Error fetching datasets:', error);
    }
  };

  return (
    <div>
      <Grid container spacing={3}>
        {datasets.map((dataset) => (
          <Grid item xs={12} sm={6} md={4} key={dataset.id}>
            <Link href={`/chart/select-chart?id=${dataset.id}`} passHref>
              <Paper component="a" elevation={3} style={{ textDecoration: 'none', cursor: 'pointer', position: 'relative' }}>
                <Card style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                  <CardContent style={{ flexGrow: 1 }}>
                    <div style={{ position: 'relative', marginBottom: '10px' }}>
                      <img src={dataset.thumbnail ? dataset.thumbnail : 'https://www.quackit.com/pix/routeburn_track/routeburn_flats_t.jpg'} alt="Dataset Thumbnail" style={{ width: '100%', height: '200px', objectFit: 'cover' }} />
                      <Typography variant="h6" component="h2" gutterBottom style={{ position: 'absolute', bottom: '10px', left: '10px', color: '#fff' }}>
                        {dataset.name}
                      </Typography>
                    </div>
                    <Typography color="textSecondary" variant="body2">
                      {dataset.description}
                    </Typography>
                  </CardContent>
                  <div style={{ borderTop: '1px solid #e0e0e0', padding: '10px' }}>
                    <Typography color="textSecondary" variant="body2">
                      Type: {dataset.type}
                    </Typography>
                    <Typography color="textSecondary" variant="body2">
                      Category: {dataset.category.name}
                    </Typography>
                  </div>
                </Card>
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default ChooseChartPage;
