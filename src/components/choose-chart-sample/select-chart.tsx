import { useRouter, usePathname, useSearchParams } from "next/navigation";
import DefaultLayout from "@/components/Layouts/DefaultLayout";
import { Card, CardContent, Typography } from "@mui/material";
import Link from "next/link";
import Breadcrumb from "@/components/Breadcrumbs/Breadcrumb";
import React from "react";

const SelectChart: React.FC = () => {
  return (
    <DefaultLayout>
      <div className="max-w-270 text-left">
        <Breadcrumb pageName="Choose Chart Type" />
      </div>
      <div>
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {/*<h2>*/}
          {/*  <strong>Choose a chart type </strong>*/}
          {/*</h2>*/}
          {/*<br />*/}
          <p>
            It is important to choose the type of visualization fits your data
            best. Choose the chart type that represents your data accurately or
            pick the recommended type if you need help deciding what type of
            chart to use.
          </p>
          <br />
          <br />

          <ChartOptionCard chartType="Bar Chart" description="Comparisons" />
          <ChartOptionCard
            chartType="Line Chart"
            description="Trends over time"
          />
          <ChartOptionCard chartType="Pie Chart" description="Proportions" />
          <ChartOptionCard
            chartType="Scatter Chart"
            description="Trends over time"
          />
          <ChartOptionCard
            chartType="Area Chart"
            description="Trends over time"
          />
          <ChartOptionCard
            chartType="TreeMap Chart"
            description="Hierarchical data"
          />
          <ChartOptionCard
            chartType="Funnel Chart"
            description="Sales process"
          />
          <ChartOptionCard chartType="Radar Chart" description="Comparisons" />
          {/* Add more chart options as needed */}
        </div>
      </div>
    </DefaultLayout>
  );
};

const ChartOptionCard: React.FC<{ chartType: string; description: string }> = ({
  chartType,
  description,
}) => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const handleChartSelection = () => {
    // Navigate to another page with selected chart type
    console.log(chartType.toLowerCase().replace(" ", "-"));
    router.push(
      `/chart/chart-workspace/${chartType.toLowerCase().replace(" ", "-")}?${searchParams}`,
    );
  };

  return (
    <Card
      key={chartType}
      style={{ margin: "10px", width: "250px", cursor: "pointer" }}
      onClick={handleChartSelection}
    >
      <CardContent style={{ textAlign: "center", height: "5%" }}>
        <img
          src={`/images/cards/${chartType.toLowerCase().replace(" ", "-")}.png`}
          alt={`${chartType} Image`}
          height={200}
          //width={250}
          style={{ marginBottom: "10px" }}
        />
        <Typography variant="h6" component="h2">
          {chartType}
          <br />
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default SelectChart;
