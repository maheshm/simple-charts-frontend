import React from "react";
import Image from "next/image";
import { Button, Card, CardContent, Grid, Typography } from "@mui/material";

const LandingPage: React.FC = () => {
  return (
    <>
      <div>
        <Grid  spacing={0}>
          <Grid item xs={12}>
            <div style={{ position: "relative", textAlign: "center" }}>
              <Image
              src="/images/landing/heading.jpeg"
              layout="responsive"
              width={800}
              height={800}
              alt="User"
              />
              <Button
                variant="contained"
                color="primary"
                style={{
                  position: "absolute",
                  top: "85%",
                  left: "25%",
                  transform: "translate(-50%, -50%)",
                  borderRadius: 20, // Adjust border radius
                  padding: "10px 20px", // Adjust padding
                  fontSize: "0.9rem", // Adjust font size
                  fontWeight: "bold", // Adjust font weight
                  boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)", // Add box shadow
                  transition: "all 0.3s ease", // Add transition effect

                }}
                href="/chart/create"
              >
                Create A Chart
              </Button>
            </div>
          </Grid>
        </Grid>
        <br />
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Card style={{ borderRadius: 20, padding: "15px 75px 0px", fontSize: "1.2rem", fontWeight: "bold", boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)", transition: "all 0.3s ease" }}>
              <CardContent style={{ textAlign: "center" }}>
                <Image
                  src="/images/landing/ChooseData.svg"
                  width={500}
                  height={500}
                  style={{
                    width: "auto",
                    height: "auto",
                    borderRadius: "50%",
                  }}
                  alt="User"
                />
                <Typography variant="h5" component="h2" style={{ textAlign: "center", fontWeight: "bold", fontSize: "1.2rem" }}>
                  Step 1
                </Typography>
                <Typography color="textSecondary" style={{
                  textAlign: "center", fontWeight: "bold", fontSize: "1.2rem"
                }}>Choose Data</Typography>
              </CardContent>

            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card style={{ borderRadius: 20, padding: "15px 75px 0px", fontSize: "1.2rem", fontWeight: "bold", boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)", transition: "all 0.3s ease" }}>
              <CardContent style={{ textAlign: "center" }}>
                <img
                  src="/images/landing/SelectChart.svg"
                  alt="Step 2 Select Chart"
                />
                <Typography variant="h5" component="h2" style={{ textAlign: "center", fontWeight: "bold", fontSize: "1.2rem" }}>
                  Step 2
                </Typography>
                <Typography color="textSecondary" style={{
                  textAlign: "center", fontWeight: "bold", fontSize: "1.2rem"
                }}>Select Chart</Typography>


              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card style={{ borderRadius: 20, padding: "15px 75px 0px", fontSize: "1.2rem", fontWeight: "bold", boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.25)", transition: "all 0.3s ease" }}>
              <CardContent style={{ textAlign: "center" }}>
                <img src="/images/landing/Customise.svg" alt="Step 3 Customize" />
                <Typography variant="h5" component="h2" style={{ textAlign: "center", fontWeight: "bold", fontSize: "1.2rem" }}>
                  Step 3
                </Typography>
                <Typography color="textSecondary" style={{
                  textAlign: "center", fontWeight: "bold", fontSize: "1.2rem"
                }}>Customise</Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default LandingPage;
