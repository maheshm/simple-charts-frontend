import Link from "next/link";
interface BreadcrumbProps {
  pageName: string;
}
const Breadcrumb = ({ pageName }: BreadcrumbProps) => {
  return (
    <div className="sm:items-left mb-6 flex flex-col sm:flex-row sm:justify-between">
      {/*<h2 className="size:10 text-center text-4xl font-semibold text-black">*/}
      {/*  {pageName}*/}
      {/*</h2>*/}

      <nav>
        <ol className="items-left flex gap-2 text-xl">
          <li>
            <Link className="font-bold" href="/">
              Dashboard /
            </Link>
          </li>
          <li className="font-bold text-primary">{pageName}</li>
        </ol>
      </nav>
    </div>
  );
};

export default Breadcrumb;
