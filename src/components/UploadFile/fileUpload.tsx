import React, { useEffect, useState } from 'react';
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from '@mui/material';
import { CloudUpload } from '@mui/icons-material';
import { usePathname } from "next/navigation";

const UploadFilePage: React.FC = () => {
  const [datasets, setDatasets] = useState<any[]>([]);
  const [name, setName] = useState('');
  const [type, setType] = useState('');
  const [description, setDescription] = useState('');
  const [categoryType, setCategoryType] = useState(0);
  const [file, setFile] = useState<File | null>(null);
  const [imageFile, setImageFile] = useState<File | null>(null);
  const [imageBase64, setImageBase64] = useState<string>('');

  const pathname = usePathname();

  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    try {
      const response = await fetch('https://simplecharts.maheshlabs.com/api/resource-category');
      const data = await response.json();
      setDatasets(data);
    } catch (error) {
      console.error('Error fetching datasets:', error);
    }
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const selectedFile = event.target.files ? event.target.files[0] : null;
    setFile(selectedFile);
  };

  const handleImageFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const selectedImageFile = event.target.files ? event.target.files[0] : null;
    setImageFile(selectedImageFile);

    // Convert image file to base64 string
    if (selectedImageFile) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setImageBase64(reader.result as string);

      };
      reader.readAsDataURL(selectedImageFile);
    }
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!file || !imageFile) {
      console.error('Please select both a file and an image');
      return;
    }
  
    const formData = new FormData();
    formData.append('file', file);
  
    try {
      const url = 'https://simplecharts.maheshlabs.com/api/datasets/upload';
 
  // add multipart form data to the request
      const response = await fetch(`${url}?name=${name}&type=${type}&description=${description}&category_id=${categoryType}&thumbnail=${imageBase64}`
      , {
        method: 'POST',
        body: formData,
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error('Error uploading file:', error);
    }
  };

  return (
    
    <Box  className="max-w-270 text-left">
      <form onSubmit={handleSubmit} className="space-y-4">
        {imageBase64 && (
          <div className="flex justify-center">
            <img src={imageBase64} alt="Uploaded Image" className="max-w-full h-auto" />
          </div>
        )}
        <TextField
          label="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          fullWidth
          required
        />
        <TextField
          label="Type"
          value={type}
          onChange={(e) => setType(e.target.value)}
          fullWidth
          required
        />
        <TextField
          label="Description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          fullWidth
          required
        />
        <FormControl fullWidth required>
          <InputLabel>Category Type</InputLabel>
          <Select
            value={categoryType}
            onChange={(e) => setCategoryType(e.target.value as number)}
          >
            {datasets.map((category) => (
              <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem> 
            ))}
          </Select>
        </FormControl>
        <input
          type="file"
          accept=".csv,.xlsx"
          onChange={handleFileChange}
          style={{ display: 'none' }}
          id="upload-file-input"
        />
        <label htmlFor="upload-file-input" className="w-full" style={{ cursor: 'pointer' , padding: '1.5rem 1', borderRadius: '4px' }}>
          <Button variant="contained" component="span" startIcon={<CloudUpload />}>
            Upload File
          </Button>
        </label>
        <input
          type="file"
          accept="image/*"
          onChange={handleImageFileChange}
          style={{ display: 'none' }}
          id="upload-image-input"
        />
        <label htmlFor="upload-image-input" className="w-full" style={{ cursor: 'pointer', padding: '0.5rem 1', borderRadius: '4px' }}>
          <Button variant="contained" component="span" startIcon={<CloudUpload />}>
            Upload Image
          </Button>
        </label>
        <br />
        <div className="flex justify-end space-x-4">
          <Button variant="contained" color="secondary">
            Cancel
          </Button>
          <Button variant="contained" color="primary" type="submit">
            Save
          </Button>
        </div>
      </form>
    </Box>
  );
};

export default UploadFilePage;
