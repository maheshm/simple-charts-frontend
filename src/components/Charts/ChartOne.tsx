import React, { useState, useEffect } from "react";
import ReactApexChart from "react-apexcharts";
import { ApexOptions } from "apexcharts";

const ChartOne: React.FC = () => {
  const [selectedFrequency, setSelectedFrequency] = useState<string>("Day");
  const [chartData, setChartData] = useState<{ category: string; frequency: number }[]>([]);

  useEffect(() => {
    fetchData(selectedFrequency);
  }, [selectedFrequency]);

  const fetchData = async (frequency: string) => {
    try {
      const email = encodeURIComponent("nikhil@vt.edu");
      const response = await fetch(`https://simplecharts.maheshlabs.com/api/analytics/chart-frequency?email=${email}&frequency=${frequency}`);
      const data = await response.json();
      setChartData(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleFrequencyChange = (frequency: string) => {
    setSelectedFrequency(frequency);
  };

  const options: ApexOptions = {
    legend: {
      show: false,
      position: "top",
      horizontalAlign: "left",
    },
    chart: {
      fontFamily: "Satoshi, sans-serif",
      height: 335,
      type: "area",
      toolbar: {
        show: false,
      },
    },
    stroke: {
      width: [2, 2],
      curve: "straight",
    },
    grid: {
      xaxis: {
        lines: {
          show: true,
        },
      },
      yaxis: {
        lines: {
          show: true,
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 4,
      colors: "#fff",
      strokeColors: ["#3056D3", "#80CAEE"],
      strokeWidth: 3,
      strokeOpacity: 0.9,
      strokeDashArray: 0,
      fillOpacity: 1,
      discrete: [],
      hover: {
        size: undefined,
        sizeOffset: 5,
      },
    },
    xaxis: {
      type: "category",
      categories: chartData.map((item) => item.category),
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      title: {
        style: {
          fontSize: "0px",
        },
      },
      min: 0,
      max: Math.max(...chartData.map((item) => item.frequency)) + 10, // Adjust maximum value dynamically
    },
  };

  return (
    <div className="col-span-12 rounded-sm border border-stroke bg-white px-5 pb-5 pt-7.5 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:col-span-8">
      <div className="mb-3 justify-between gap-4 sm:flex">
        <div>
          <h5 className="text-xl font-semibold text-black dark:text-white">
            Charts Frequency by {selectedFrequency}
          </h5>
        </div>
      </div>
      <div className="flex flex-wrap items-start justify-between gap-3 sm:flex-nowrap">
        {/* Frequency selection dropdown */}
        <div className="flex w-full max-w-45 justify-end">
          <div className="inline-flex items-center rounded-md bg-whiter p-1.5 dark:bg-meta-4">
            <button
              className={`rounded px-3 py-1 text-xs font-medium ${
                selectedFrequency === "Day" ? "bg-white text-black shadow-card" : "hover:bg-white hover:shadow-card dark:text-white dark:hover:bg-boxdark"
              }`}
              onClick={() => handleFrequencyChange("Day")}
            >
              Day
            </button>
            <button
              className={`rounded px-3 py-1 text-xs font-medium ${
                selectedFrequency === "Month" ? "bg-white text-black shadow-card" : "hover:bg-white hover:shadow-card dark:text-white dark:hover:bg-boxdark"
              }`}
              onClick={() => handleFrequencyChange("Month")}
            >
              Month
            </button>
            <button
              className={`rounded px-3 py-1 text-xs font-medium ${
                selectedFrequency === "Year" ? "bg-white text-black shadow-card" : "hover:bg-white hover:shadow-card dark:text-white dark:hover:bg-boxdark"
              }`}
              onClick={() => handleFrequencyChange("Year")}
            >
              Year
            </button>
          </div>
        </div>
      </div>

      <div>
        <div id="chartOne" className="-ml-5">
          <ReactApexChart
            options={options}
            series={[{ name: "Frequency", data: chartData.map((item) => item.frequency) }]}
            type="area"
            height={350}
            width={"100%"}
          />
        </div>
      </div>
    </div>
  );
};

export default ChartOne;
