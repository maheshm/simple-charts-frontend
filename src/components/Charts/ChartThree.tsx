import React, { useState, useEffect } from "react";
import {
  PieChart,
  Pie,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Cell,
} from "recharts";

const ChartThree: React.FC = () => {
  const [filteredData, setFilteredData] = useState<{ name: string; value: number }[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const email = localStorage.getItem("email") || "nikhil@vt.edu";
        const response = await fetch(`https://simplecharts.maheshlabs.com/api/analytics/chart-type-frequency?email=${email}`);
        const data = await response.json();
        if (data && data.length > 0) {
          const groupedData = data.reduce((accumulator: any, currentValue: any) => {
            const xValue = currentValue.category.toLowerCase();
            const yValue = currentValue.frequency;

            if (!accumulator[xValue]) {
              accumulator[xValue] = 0;
            }
            accumulator[xValue] += yValue;

            return accumulator;
          }, {});

          const formattedData = Object.keys(groupedData).map((category) => ({
            name: category,
            value: groupedData[category],
          }));

          setFilteredData(formattedData);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);
  const colors = ["#8884d8", "#82ca9d", "#ffc658", "#ff7f0e", "#ffbb78", "#2ca02c", "#d62728", "#9467bd", "#c5b0d5", "#8c564b"];


  return (
    <div className="col-span-12 rounded-sm border border-stroke bg-white px-5 pb-5 pt-7.5 shadow-default dark:border-strokedark dark:bg-boxdark sm:px-7.5 xl:col-span-5" style={{ height: '491px' }}>
      <div className="mb-3 justify-between gap-4 sm:flex">
        <div>
          <h5 className="text-xl font-semibold text-black dark:text-white">
            Charts Frequency
          </h5>
        </div>
      </div>
      <div className="mb-2">
        <div id="chartThree" className="mx-auto flex justify-center">
          <ResponsiveContainer width="100%" height={300}>
            <PieChart>
              <Pie
                data={filteredData}
                dataKey="value"
                nameKey="name"
                cx="50%"
                cy="50%"
                outerRadius={100}
                label
              >
                {filteredData.map((entry, index) => (
                  <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
                ))}
              </Pie>
              <Tooltip />
              <Legend />
            </PieChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
};

export default ChartThree;
