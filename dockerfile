# Use the official Node.js 18 image as a base
FROM node:18-alpine

WORKDIR /app

COPY package.json .
COPY package-lock.json .

# Install dependencies
RUN npm install

COPY . .


RUN npm run build

EXPOSE 3000

# Command to run the Next.js application
CMD ["npm", "start"]
