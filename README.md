#Simple Charts Front End

@dev Nikhil Ram

TailAdmin utilizes the powerful features of **Next.js 13** and common features of Next.js such as server-side rendering (SSR), static site generation (SSG), and seamless API route integration. Combined with the advancements of **React 18** and the robustness of **TypeScript**, TailAdmin is the perfect solution to help get your project up and running quickly.


#Installation
```
npm install
```
or

```
yarn install
```

3. Now run this command to start the developement server

```
npm run dev
```

or 

```
yarn dev
```
